#version 150 core

// couleur émise pour le pixel
in vec4 color;
in vec3 lightDir;
in vec3 eyeVec;
in vec3 out_normal;
out vec4 frag_color;
out vec4 final_color;


void main( void )
{
    vec3 L = normalize(lightDir);
    vec3 N = normalize(out_normal);
    float intensity = max(dot(L,N),0.0);
    vec4 final_color =vec4(0.2,0.2,0.2, 1.0);


    final_color +=0.6*intensity;


    frag_color = final_color;
}
