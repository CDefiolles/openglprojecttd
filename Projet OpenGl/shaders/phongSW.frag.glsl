#version 150 core

// couleur émise pour le pixel
in vec4 color;
in vec3 lightDir;
in vec3 eyeVec;
in vec3 out_normal;
out vec4 frag_color;
out vec4 final_color;

void main(void)
{
    // la couleur du pixel est verte
    vec3 L = normalize(lightDir);
    vec3 N = normalize(out_normal);
    vec3 E = normalize(eyeVec);
    vec3 R = reflect(-L, N);
    float specular = pow(max(dot(R, E), 0.005),1.0);
    final_color= vec4(10,7.5,9.5,1)*specular;


    frag_color = final_color;

}
