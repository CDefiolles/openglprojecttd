#include <iostream>
#include <vector>
#include <array>
#include <fstream>

#if defined(__APPLE__)
#include <OpenGL/gl3.h>
#include <GLUT/glut.h>

#else
#include <GL/glew.h>
#include <GL/glut.h>
#include <GL/freeglut.h>
#endif

#include "Config.h"

#define ENABLE_SHADERS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
//#include <config.h>  //tuto LearnOpenGl


unsigned int mvpid,mid,pid,vid;

// Matrices 4x4 contenant les transformations.
glm::mat4 model;
glm::mat4 view;
glm::mat4 proj ;
glm::mat4 mvp;


float scale=0.1f;
float decal=1.25f;
int lastx = 0;
int lasty = 0;

const float YAW = 0.0f;
const float PITCH = 0.0f;
const float SPEED = 0.05f;

/*Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
float lastX = SCR_WIDTH / 2.0f;                     //tuto LearnOpenGl
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;*/ 

constexpr int NBMESHES=4;


struct Shader
{
    unsigned int progid; // ID du shader
    unsigned int mid; // ID de la matrice de modelisation qui est passée en variable uniform au shader de vertex
    unsigned int vid;
    unsigned int pid;
    unsigned int LightID;

    void Bind() const { glUseProgram(progid); }
    void Unbind() const { glUseProgram(0); }
}shaders[NBMESHES];


struct Maillage
{
    Shader shader;
    unsigned int vaoids; // VaoID qui contient les VBO où ce trouve ce maillage (et ses normales etc..) dans la VRAM
    unsigned int nbtriangles;
    float angle = 0.1f;
    float scale = 0.0f; // stocke l'opération de normalisation de la taille du maillage (réalisé lors de la lecture du maillage)
    float inc = 0.01f;
    float x, y, z; // stocke la position du centre du  maillage (réalisé lors de la lecture du maillage) il faut utiliser cette info lors du dessin afin de toujours veiller à recentrer le maillage en (0,0,0)

} maillages[NBMESHES];


void displayMesh(const Maillage& maillage, const glm::mat4& model)
{
    glUseProgram( maillage.shader.progid );
    glBindVertexArray(maillage.vaoids);

    //maillage.shader.Bind();
    glUniformMatrix4fv(maillage.shader.mid, 1, false, glm::value_ptr(model));
    glUniformMatrix4fv(maillage.shader.vid, 1, false, glm::value_ptr(view));
    glUniformMatrix4fv(maillage.shader.pid, 1, false, glm::value_ptr(proj));

    glBindVertexArray(maillage.vaoids);
    glDrawElements(GL_TRIANGLES, maillage.nbtriangles*3, GL_UNSIGNED_INT, 0);


    //maillage.shader.Unbind();
}
#include <sys/timeb.h>

int getMilliCount(){
    timeb tb;
    ftime(&tb);
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int getMilliSpan(int nTimeStart){
    int nSpan = getMilliCount() - nTimeStart;
    if(nSpan < 0)
        nSpan += 0x100000 * 1000;
    return nSpan;
}

int lastTime=0;
double elapsed;

struct camera
{
    glm::vec3 Position={ 0.0f, 0.0f, 5.0f };
    glm::vec3 Front={ 0.0f, 0.0f, -1.0f };
    glm::vec3 Up;
    glm::vec3 Right;
    glm::vec3 WorldUp={ 0.0f, 1.0f, 0.0f };;
    // euler Angles
    float Yaw=YAW;
    float Pitch=PITCH;
    // camera options
    float MovementSpeed=SPEED;

} globalcamera;


void display()
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
        view = glm::lookAt( globalcamera.Position,globalcamera.Position+globalcamera.Front,globalcamera.WorldUp);
    //view = glm::rotate( view, glm::degrees( angle ), glm::vec3( 0.0f, 1.0f, 0.0f ) );


    model = glm::mat4( 1.0f );
    model = glm::translate( model, glm::vec3( -decal, -decal, 1.0f ) );
    model=glm::rotate(model,glm::degrees(maillages[0].angle/35),glm::vec3(1.0f,0.0f,0.0f));
    model = glm::scale(   model, glm::vec3( maillages[0].scale) );
    displayMesh(maillages[0], model);

    model = glm::mat4( 1.0f );
    model = glm::translate( model, glm::vec3( decal, decal-0.3f, 2.0f ) );
    model=glm::rotate(model,glm::degrees(maillages[1].angle/35),glm::vec3(0.0f,0.1f,0.0f));
    model = glm::scale(   model, glm::vec3( maillages[1].scale) );
    displayMesh(maillages[1], model);

    model = glm::mat4( 1.0f );
    model = glm::translate( model, glm::vec3( -decal, decal-0.6f, 2.0f ) );
    model=glm::rotate(model,glm::degrees(maillages[2].angle/35),glm::vec3(0.0f,0.1f,0.0f));
    model = glm::scale(   model, glm::vec3( maillages[2].scale) );
    displayMesh(maillages[2],model);


    model = glm::mat4( 1.0f );
    model = glm::translate( model, glm::vec3( decal+0.5f, -decal-1.0f, 0.5f ) );
    model=  glm::rotate(model,glm::degrees(maillages[3].angle/25),glm::vec3(0.0f,-1.0f,0.0f));
    model = glm::scale(   model, glm::vec3( maillages[3].scale) );
    displayMesh(maillages[3], model);

    glutSwapBuffers();
}


void idle()
{
    for ( auto & maillage : maillages) {
        maillage.angle +=maillage.inc;
        if( maillage.angle >= 360.0f )
        {
            maillage.angle = 0.0f;
        }
    }
    glutPostRedisplay();
}


void reshape( int w, int h )
{
    glViewport(0, 0, w, h);
    // Modification de la matrice de projection à chaque redimensionnement de la fenêtre.
    proj = glm::perspective( 45.0f, w/static_cast< float >( h ), 0.1f, 5.0f );
}



void updateCameraVectors(camera *globalcamera,int x,int y){
    int diffx = x - lastx;
    int diffy = y - lasty;
    lastx = x;
    lasty = y;

    globalcamera->Yaw -=  globalcamera->MovementSpeed* 0.1f * diffx;
    globalcamera->Pitch -= globalcamera->MovementSpeed * 0.1f * diffy;

    if (globalcamera->Pitch > (float)M_PI / 2.0f - 0.1f)
        globalcamera->Pitch = (float)M_PI / 2.0f - 0.1f;
    if (globalcamera->Pitch < -(float)M_PI / 2.0f + 0.1f)
        globalcamera->Pitch = -(float)M_PI / 2.0f + 0.1f;
    glm::vec3 front;
    front.x = -std::sin( globalcamera->Yaw ) * std::cos(globalcamera->Pitch);
    front.y = std::sin(globalcamera->Pitch);
    front.z =-std::cos(globalcamera->Yaw) * std::cos(globalcamera->Pitch);
    globalcamera->Front = glm::normalize(front);
    globalcamera->Right = glm::normalize( glm::cross( globalcamera->Front, globalcamera->WorldUp ) );
    globalcamera->Up = glm::normalize( glm::cross( globalcamera->Right, globalcamera->Front ) );

    // std::cout << "(" << x << ", " << y << ")" << std::endl;
    glutPostRedisplay();}



void keyboard(unsigned char key, int x, int y) {
    switch( key )
    {

        case 'z':
            globalcamera.Position+=globalcamera.Front*globalcamera.MovementSpeed;
            globalcamera.Pitch = std::min( (float)M_PI / 2.0f - 0.1f, globalcamera.Pitch );
            break;
        case 's':
            globalcamera.Position-=globalcamera.Front*globalcamera.MovementSpeed;
            globalcamera.Pitch = std::max( -(float)M_PI / 2.0f + 0.1f, globalcamera.Pitch );
            break;
        case 'q':
            globalcamera.Position-=globalcamera.Right*globalcamera.MovementSpeed;

            break;
        case 'd':
            globalcamera.Position+=globalcamera.Right*globalcamera.MovementSpeed;
            //calcTime();

            break;
    }
    updateCameraVectors(&globalcamera,x,y);
    glutPostRedisplay();
 }




Maillage initVAOs(const Shader& shader, const std::string& modelPath)
{
    Maillage m;
    m.shader=shader;
    unsigned int vboids[ 4 ];



    std::ifstream ifs(modelPath);

    if(!ifs.is_open()) std::cerr << "Impossible d'ouvrir le fichier " << modelPath << '\n';

    std::string off;

    unsigned int nbpoints, tmp;

    ifs >> off;
    ifs >> nbpoints;
    ifs >> m.nbtriangles;
    ifs >> tmp;

    std::vector< float > vertices( nbpoints * 3 );
    std::vector< float > colors( nbpoints * 3 );
    std::vector< unsigned int > indices( m.nbtriangles *3 );
    std::vector< float > normals( nbpoints * 3 );

    //std::fill( std::begin( normals ), std::end( normals ), 0.0f );

    for( unsigned int i = 0 ; i < vertices.size() ; ++i) {
        ifs >> vertices[ i ];
    }

    for( unsigned int i = 0 ; i < m.nbtriangles ; ++i) {
        ifs >> tmp;
        ifs >> indices[ i * 3 ];
        ifs >> indices[ i * 3 + 1 ];
        ifs >> indices[ i * 3 + 2 ];
    }

    /**
     * Calcul de la boîte englobante du modèle
     */
    float dx, dy, dz;
    float xmin, xmax, ymin, ymax, zmin, zmax;

    xmin = xmax = vertices[0];
    ymin = ymax = vertices[1];
    zmin = zmax = vertices[2];
    for(unsigned int i = 1 ; i < nbpoints ; ++i) {
        if(xmin > vertices[i*3]) xmin = vertices[i*3];
        if(xmax < vertices[i*3]) xmax = vertices[i*3];
        if(ymin > vertices[i*3+1]) ymin = vertices[i*3+1];
        if(ymax < vertices[i*3+1]) ymax = vertices[i*3+1];
        if(zmin > vertices[i*3+2]) zmin = vertices[i*3+2];
        if(zmax < vertices[i*3+2]) zmax = vertices[i*3+2];
    }

    // calcul du centre de la boîte englobante

    m.x = (xmax + xmin)/2.0f;
    m.y = (ymax + ymin)/2.0f;
    m.z = (zmax + zmin)/2.0f;

    // calcul des dimensions de la boîte englobante

    dx = xmax - xmin;
    dy = ymax - ymin;
    dz = zmax - zmin;

    // calcul du coefficient de mise à l'échelle

    scale = 1.0f/fmax(dx, fmax(dy, dz));
    m.scale=scale;
    // Calcul des normales.
    for( std::size_t i = 0 ; i < indices.size() ; i+=3 )
    {
        auto x0 = vertices[ 3 * indices [ i ]     ] - vertices[ 3 * indices [ i+1 ]     ];
        auto y0 = vertices[ 3 * indices [ i ] + 1 ] - vertices[ 3 * indices [ i+1 ] + 1 ];
        auto z0 = vertices[ 3 * indices [ i ] + 2 ] - vertices[ 3 * indices [ i+1 ] + 2 ];

        auto x1 = vertices[ 3 * indices [ i ]     ] - vertices[ 3 * indices [ i+2 ]     ];
        auto y1 = vertices[ 3 * indices [ i ] + 1 ] - vertices[ 3 * indices [ i+2 ] + 1 ];
        auto z1 = vertices[ 3 * indices [ i ] + 2 ] - vertices[ 3 * indices [ i+2 ] + 2 ];

        auto x01 = y0 * z1 - y1 * z0;
        auto y01 = z0 * x1 - z1 * x0;
        auto z01 = x0 * y1 - x1 * y0;

        auto norminv = 1.0f / std::sqrt( x01 * x01 + y01 * y01 + z01 * z01 );
        x01 *= norminv;
        y01 *= norminv;
        z01 *= norminv;

        normals[ 3 * indices[ i ]     ] += x01;
        normals[ 3 * indices[ i ] + 1 ] += y01;
        normals[ 3 * indices[ i ] + 2 ] += z01;

        normals[ 3 * indices[ i + 1 ]     ] += x01;
        normals[ 3 * indices[ i + 1 ] + 1 ] += y01;
        normals[ 3 * indices[ i + 1 ] + 2 ] += z01;

        normals[ 3 * indices[ i + 2 ]     ] += x01;
        normals[ 3 * indices[ i + 2 ] + 1 ] += y01;
        normals[ 3 * indices[ i + 2 ] + 2 ] += z01;
    }

    for( std::size_t i = 0 ; i < normals.size() ; i+=3 )
    {
        auto & x = normals[ i     ];
        auto & y = normals[ i + 1 ];
        auto & z = normals[ i + 2 ];

        auto norminv = 1.0f / std::sqrt( x * x + y * y + z * z );

        x *= norminv;
        y *= norminv;
        z *= norminv;
    }

    // Génération d'un Vertex Array Object contenant 3 Vertex Buffer Objects.
    glGenVertexArrays( 1, &m.vaoids );
    glBindVertexArray( m.vaoids );

    // Génération de 4 VBO.
    glGenBuffers( 4, vboids );

    // VBO contenant les sommets.

    glBindBuffer( GL_ARRAY_BUFFER, vboids[ 0 ] );
    glBufferData( GL_ARRAY_BUFFER, vertices.size() * sizeof( float ), vertices.data(), GL_STATIC_DRAW );

    // L'attribut in_pos du vertex shader est associé aux données de ce VBO.
    auto pos = glGetAttribLocation( m.shader.progid, "in_pos" );
    glVertexAttribPointer( pos, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray( pos );

    // VBO contenant les couleurs.

    glBindBuffer( GL_ARRAY_BUFFER, vboids[ 1 ] );
    glBufferData( GL_ARRAY_BUFFER, colors.size() * sizeof( float ), colors.data(), GL_STATIC_DRAW );

    // L'attribut in_color du vertex shader est associé aux données de ce VBO.
    auto color = glGetAttribLocation( m.shader.progid, "in_color" );
    glVertexAttribPointer( color, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray( color );

    // VBO contenant les indices.

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, vboids[ 1 ] );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof( unsigned int ), indices.data(), GL_STATIC_DRAW );

    // VBO contenant les normales.

    glBindBuffer( GL_ARRAY_BUFFER, vboids[ 3 ] );
    glBufferData( GL_ARRAY_BUFFER, normals.size() * sizeof( float ), normals.data(), GL_STATIC_DRAW );

    auto normal = glGetAttribLocation( m.shader.progid, "in_normal" );
    glVertexAttribPointer( normal, 3, GL_FLOAT, GL_TRUE, 0, 0 );
    glEnableVertexAttribArray( normal );


    return m;
}


Shader initShaders(const std::string& vertexShaderPath, const std::string& fragmentShaderPath)
{
    Shader S;
    unsigned int vsid, fsid;
    int status;
    int logsize;
    std::string log;

    std::ifstream vs_ifs( MY_SHADER_PATH+vertexShaderPath) ;
    std::ifstream fs_ifs( MY_SHADER_PATH+fragmentShaderPath) ;



    auto begin = vs_ifs.tellg();
    vs_ifs.seekg( 0, std::ios::end );
    auto end = vs_ifs.tellg();
    vs_ifs.seekg( 0, std::ios::beg );
    auto size = end - begin;

    std::string vs;
    vs.resize( size );
    vs_ifs.read( &vs[ 0 ], size );

    begin = fs_ifs.tellg();
    fs_ifs.seekg( 0, std::ios::end );
    end = fs_ifs.tellg();
    fs_ifs.seekg( 0, std::ios::beg );
    size = end - begin;

    std::string fs;
    fs.resize( size );
    fs_ifs.read( &fs[0], size );

    vsid = glCreateShader( GL_VERTEX_SHADER );
    char const * vs_char = vs.c_str();
    glShaderSource( vsid, 1, &vs_char, nullptr );
    glCompileShader( vsid );

    // Get shader compilation status.
    glGetShaderiv( vsid, GL_COMPILE_STATUS, &status );

    if( !status )
    {
        std::cerr << "Error: vertex shader compilation failed.\n";
        glGetShaderiv( vsid, GL_INFO_LOG_LENGTH, &logsize );
        log.resize( logsize );
        glGetShaderInfoLog( vsid, log.size(), &logsize, &log[0] );
        std::cerr << log << std::endl;
    }

    fsid = glCreateShader( GL_FRAGMENT_SHADER );
    char const * fs_char = fs.c_str();
    glShaderSource( fsid, 1, &fs_char, nullptr );
    glCompileShader( fsid );

    // Get shader compilation status.
    glGetShaderiv( fsid, GL_COMPILE_STATUS, &status );

    if( !status )
    {
        std::cerr << "Error: fragment shader compilation failed.\n";
        glGetShaderiv( fsid, GL_INFO_LOG_LENGTH, &logsize );
        log.resize( logsize );
        glGetShaderInfoLog( fsid, log.size(), &logsize, &log[0] );
        std::cerr << log << std::endl;
    }

    S.progid = glCreateProgram();

    glAttachShader( S.progid, vsid );
    glAttachShader( S.progid, fsid );

    glLinkProgram( S.progid );

    glUseProgram( S.progid );
    S.mid = glGetUniformLocation( S.progid, "m" );
    S.vid = glGetUniformLocation( S.progid, "v" );
    S.pid = glGetUniformLocation( S.progid, "p" );
    //mvpid = glGetUniformLocation( progid, "mvp" );
    //return progid,mid,vid,pid;
    return S;
}



int main( int argc, char * argv[] )
{

    glutInit( &argc, argv );
#if defined(__APPLE__) && defined(ENABLE_SHADERS)
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA|GLUT_3_2_CORE_PROFILE);
#else

    glutInitContextVersion( 3, 2 );
    //glutInitContextVersion( 4, 5 );
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glewInit();
#endif

    glutInitWindowSize( 860, 640 );


    glutCreateWindow( argv[ 0 ]  );

    glutDisplayFunc( display );
    glutReshapeFunc( reshape );
    glutIdleFunc( idle );

    glutKeyboardFunc( keyboard );
    glutMotionFunc(reinterpret_cast<void (*)(int, int)>(updateCameraVectors));

    // Initialisation de la bibliothèque GLEW.
#if not defined(__APPLE__)
    glewInit();
#endif

    glEnable(GL_DEPTH_TEST);

    shaders[0]=initShaders("/shaders/phong.vert.glsl", "/shaders/basic.frag.glsl");
    maillages[0]=initVAOs(shaders[0],MY_MESHES_PATH"/meshes/space_shuttle2.off");

    shaders[1]=initShaders("/shaders/phong.vert.glsl","/shaders/phongStation.frag.glsl");
    maillages[1]=initVAOs(shaders[1],MY_MESHES_PATH"/meshes/space_station2.off");

    shaders[2]=initShaders("/shaders/phong.vert.glsl","/shaders/phongSW.frag.glsl");
    maillages[2]=initVAOs(shaders[2],MY_MESHES_PATH"/meshes/ImperialShuttle.off");

    shaders[3]=initShaders("/shaders/phong.vert.glsl","/shaders/toon.frag.glsl");
    maillages[3]=initVAOs(shaders[3],MY_MESHES_PATH"/meshes/rabbit.off");
    glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );

    glutMainLoop();

    return 0;
}
